const app = require("../app");
const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;

// HOOKS
// prepare the database before tests start
beforeAll((done) => {
  queryInterface
    .bulkInsert(
      "Todos",
      [
        {
          id: 2,
          title: "Complete Weekly Reports",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          title: "Grocery Shopping",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          title: "Finish Coding Project",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          title: "Plan Weekend Getaway",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 6,
          title: "Call Client for Meeting",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
    .then((_) => {
      done();
    })
    .catch((error) => {
      console.log(error);
      done(error);
    });
});

// clean up the database after all tests are done
afterAll((done) => {
  queryInterface
    .bulkDelete("Todos", null, {})
    .then((_) => {
      done();
    })
    .catch((error) => {
      console.log(error);
      done(error);
    });
});

// test suite
describe("Todo List API Testing", () => {
  it("Get all todos", (done) => {
    request(app)
      .get("/todos")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body } = response;

        expect(body.length).toBe(5);

        const firstTodo = body[0];
        expect(firstTodo.title).toBe("Complete Weekly Reports");

        done();
      })
      .catch((error) => {
        console.log(error);
        done(error);
      });
  });

  it("Get a todo by ID", (done) => {
    request(app)
      .get("/todos/2")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body } = response;

        expect(body.title).toBe("Complete Weekly Reports");

        done();
      })
      .catch((error) => {
        console.log(error);
        done(error);
      });
  });

  it("Create a new todo", (done) => {
    request(app)
      .post("/todos")
      .send({
        title: "Pay Utility Bills",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(201)
      .then((response) => {
        const { body } = response;

        expect(body.message).toBe("Todo record created successfully.");

        done();
      })
      .catch((error) => {
        console.log(error);
        done(error);
      });
  });

  it("Update a todo", (done) => {
    request(app)
      .put("/todos/2")
      .send({
        title: "Organize Desk Area",
      })
      .set("Accept", "application/json")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body } = response;

        expect(body.message).toBe("Data updated successfully.");

        done();
      })
      .catch((error) => {
        console.log(error);
        done(error);
      });
  });

  it("Delete a todo", (done) => {
    request(app)
      .delete("/todos/2")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((response) => {
        const { body } = response;

        expect(body.message).toBe("Data deleted successfully.");

        done();
      })
      .catch((error) => {
        console.log(error);
        done(error);
      });
  });
});
