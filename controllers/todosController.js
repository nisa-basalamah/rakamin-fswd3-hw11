const { Todo } = require("../models");

class TodoController {
  static findAll = async (request, response, next) => {
    try {
      const todos = await Todo.findAll();
      response.status(200).json(todos);
    } catch (error) {
      next(error);
    }
  };

  static findOne = async (request, response, next) => {
    try {
      const { id } = request.params;
      const todo = await Todo.findByPk(id);

      if (!todo) {
        throw { name: "ErrorNotFound" };
      }
      response.status(200).json(todo);
    } catch (error) {
      next(error);
    }
  };

  static create = async (request, response, next) => {
    try {
      const { title } = request.body;
      const payload = { title };
      const todo = await Todo.create(payload);

      response.status(201).json({
        message: "Todo record created successfully.",
      });
    } catch (error) {
      next(error);
    }
  };

  static update = async (request, response, next) => {
    try {
      const { id } = request.params;
      const { title } = request.body;
      const payload = { title };

      const todo = await Todo.update(payload, {
        where: {
          id,
        },
      });

      if (!todo[0]) {
        throw { name: "ErrorNotFound" };
      }
      response.status(200).json({ message: "Data updated successfully." });
    } catch (error) {
      next(error);
    }
  };

  static delete = async (request, response, next) => {
    try {
      const { id } = request.params;
      const todo = await Todo.destroy({
        where: {
          id,
        },
      });

      if (!todo) {
        throw { name: "ErrorNotFound" };
      }
      response.status(200).json({ message: "Data deleted successfully." });
    } catch (error) {
      next(error);
    }
  };
}

module.exports = TodoController;
