"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      "Todos",
      [
        {
          title: "Complete Weekly Reports",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Grocery Shopping",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Finish Coding Project",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Plan Weekend Getaway",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Call Client for Meeting",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Todos", null, {});
  },
};
