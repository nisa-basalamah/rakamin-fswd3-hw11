const errorHandler = (error, request, response, next) => {
  if (error.name === "ErrorNotFound") {
    response.status(404).json({
      message: "The requested data could not be found.",
    });
  } else if (error.name === "SequelizeValidationError") {
    response.status(400).json({
      message: "Validation error.",
    });
  } else {
    response.status(500).json({
      message: "Internal server error.",
    });
  }
};

module.exports = errorHandler;
