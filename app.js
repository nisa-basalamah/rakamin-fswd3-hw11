require("dotenv").config();
const express = require("express");
const app = express();
const morgan = require("morgan");
const router = require("./routes");
const errorHandler = require("./middlewares/errorHandler.js");

app.use(morgan("tiny"));
app.use(express.json());
app.use(router);
app.use(errorHandler);

if (!["test", "test_docker"].includes(process.env.NODE_ENV)) {
  app.listen(process.env.PORT, () => {
    console.log(`Server is running and listening on port ${process.env.PORT}`);
  });
}

module.exports = app;
